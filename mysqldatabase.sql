CREATE DATABASE andysite;


CREATE TABLE IF NOT EXISTS `andysite`.`portfolio` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `project` VARCHAR(255) NOT NULL,
  `client` VARCHAR(255) NOT NULL,
  `photo` VARCHAR(4000) NOT NULL, 
  `description` VARCHAR(4000) NOT NULL,
  `tag1` VARCHAR(255) NULL,
  `tag2` VARCHAR(255) NULL,
  `tag3` VARCHAR(255) NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


LOCK TABLES `portfolio` WRITE;
INSERT INTO `portfolio` VALUES 
(1,
"Blog Re-Design", 
"The Campfire Collective", 
"https://i.postimg.cc/0r1MbHMV/campfire.jpg", 
"Prepared animated and functional prototype for blog that outlined design, content organization and functionality using Sketch and Invision <br/> Interpreted strategic brand needs into a simplified filtering user interface <br/> Designed page layouts that met the User Experience needs",
"UX",
"UI",
"Project Management");
UNLOCK TABLES;

